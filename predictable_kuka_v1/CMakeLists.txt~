cmake_minimum_required(VERSION 2.8.3)
project(dmp_bbo_node)

option(BUILD_SHARED_LIBS "Enable this option to build shared libraries" OFF)

if(BUILD_SHARED_LIBS)
set(SHARED_OR_STATIC "SHARED")
else()
set(SHARED_OR_STATIC "STATIC")
endif()


#set(CMAKE_CXX_FLAGS "-O0 -ggdb")
set(CMAKE_CXX_FLAGS_DEBUG "-O0 -ggdb")
#set(CMAKE_CXX_FLAGS_RELEASE "-O3 -DNDEBUG")
set(CMAKE_CXX_FLAGS_RELEASE "-O0 -ggdb")

#find_package(SDL2 REQUIRED)
include(CheckCXXCompilerFlag)
CHECK_CXX_COMPILER_FLAG("-std=c++11" COMPILER_SUPPORTS_CXX11)
CHECK_CXX_COMPILER_FLAG("-std=c++0x" COMPILER_SUPPORTS_CXX0X)
if(COMPILER_SUPPORTS_CXX11)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
elseif(COMPILER_SUPPORTS_CXX0X)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x")
else()
        message(STATUS "The compiler ${CMAKE_CXX_COMPILER} has no C++11 support. Please use a different C++ compiler.")
endif()

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS
  roscpp
  rospy
  std_msgs
keyboard
message_generation
)

find_package( Boost 1.34 COMPONENTS filesystem system serialization iostreams REQUIRED)

#set(SDL2_Flags "-mwindows -Wl,--no-undefined -static-libgcc")
# library paths
#set(SDL2_ROOT     "/home/hrcstudents/Documents/SDL-1.2")
#set(SDL2_Includes "${SDL2_ROOT}/include")
#set(SDL2_LibDir   "${SDL2_ROOT}/lib")

include_directories(${CMAKE_SOURCE_DIR}/src)
include_directories(${CMAKE_SOURCE_DIR}/include)

set(bbo src/DistributionGaussian.cpp 
#src/runEvolutionaryOptimization.cpp 
src/UpdateSummary.cpp 
src/UpdaterCovarDecay.cpp 
src/UpdaterMean.cpp)

set(dmp src/Dmp.cpp
src/Trajectory.cpp)

set(dmp_bbo src/TaskSolverDmp.cpp
src/UpdateSummaryParallel.cpp
src/runEvolutionaryOptimization.cpp)

set(TaskViapoint src/TaskViapoint.cpp)

set(dynamicalsystems src/DynamicalSystem.cpp
src/ExponentialSystem.cpp
src/SigmoidSystem.cpp
src/SpringDamperSystem.cpp
src/TimeSystem.cpp)

set(functionapproximators src/BasisFunction.cpp
src/FunctionApproximator.cpp
src/FunctionApproximatorLWR.cpp
src/FunctionApproximatorRBFN.cpp
src/MetaParameters.cpp
src/MetaParametersLWR.cpp
src/MetaParametersRBFN.cpp
src/ModelParameters.cpp
src/ModelParametersLWR.cpp
src/ModelParametersRBFN.cpp
src/Parameterizable.cpp
src/UnifiedModel.cpp)





find_package(SDL REQUIRED)

set(LIBS ${SDL_LIBRARY})





set(HEADERS include/${PROJECT_NAME}/BasisFunction.hpp
include/${PROJECT_NAME}/BoostSerializationToString.hpp
include/${PROJECT_NAME}/DistributionGaussian.hpp
include/${PROJECT_NAME}/Dmp.hpp
include/${PROJECT_NAME}/DynamicalSystem.hpp
include/${PROJECT_NAME}/EigenBoostSerialization.hpp
include/${PROJECT_NAME}/EigenBoostSerialization.tpp
include/${PROJECT_NAME}/EigenFileIO.hpp
include/${PROJECT_NAME}/EigenFileIO.tpp
include/${PROJECT_NAME}/ExponentialSystem.hpp
include/${PROJECT_NAME}/FunctionApproximator.hpp
include/${PROJECT_NAME}/FunctionApproximatorLWR.hpp
include/${PROJECT_NAME}/FunctionApproximatorRBFN.hpp
include/${PROJECT_NAME}/getFunctionApproximatorByName.hpp
include/${PROJECT_NAME}/gnuplot-iostream.h
include/${PROJECT_NAME}/MetaParameters.hpp
include/${PROJECT_NAME}/MetaParametersLWR.hpp
include/${PROJECT_NAME}/MetaParametersRBFN
include/${PROJECT_NAME}/ModelParameters
include/${PROJECT_NAME}/ModelParametersLWR
include/${PROJECT_NAME}/ModelParametersRBFN
include/${PROJECT_NAME}/Parameterizable.hpp
include/${PROJECT_NAME}/runEvolutionaryOptimization.hpp
include/${PROJECT_NAME}/SigmoidSystem.hpp
include/${PROJECT_NAME}/SpringDamperSystem.hpp
include/${PROJECT_NAME}/Task.hpp
include/${PROJECT_NAME}/TaskSolver.hpp
include/${PROJECT_NAME}/TaskSolverDmp.hpp
include/${PROJECT_NAME}/TaskViapoint.hpp
include/${PROJECT_NAME}/TimeSystem.hpp
include/${PROJECT_NAME}/Trajectory.hpp
include/${PROJECT_NAME}/Updater.hpp
include/${PROJECT_NAME}/UpdaterCovarDecay.hpp
include/${PROJECT_NAME}/UpdaterMean.hpp
include/${PROJECT_NAME}/UpdateSummary.hpp
include/${PROJECT_NAME}/UpdateSummaryParallel.hpp
include/${PROJECT_NAME}/keyboard.h)

















## System dependencies are found with CMake's conventions
# find_package(Boost REQUIRED COMPONENTS system)


## Uncomment this if the package has a setup.py. This macro ensures
## modules and global scripts declared therein get installed
## See http://ros.org/doc/api/catkin/html/user_guide/setup_dot_py.html
# catkin_python_setup()

################################################
## Declare ROS messages, services and actions ##
################################################

## To declare and build messages, services or actions from within this
## package, follow these steps:
## * Let MSG_DEP_SET be the set of packages whose message types you use in
##   your messages/services/actions (e.g. std_msgs, actionlib_msgs, ...).
## * In the file package.xml:
##   * add a build_depend tag for "message_generation"
##   * add a build_depend and a run_depend tag for each package in MSG_DEP_SET
##   * If MSG_DEP_SET isn't empty the following dependency has been pulled in
##     but can be declared for certainty nonetheless:
##     * add a run_depend tag for "message_runtime"
## * In this file (CMakeLists.txt):
##   * add "message_generation" and every package in MSG_DEP_SET to
##     find_package(catkin REQUIRED COMPONENTS ...)
##   * add "message_runtime" and every package in MSG_DEP_SET to
##     catkin_package(CATKIN_DEPENDS ...)
##   * uncomment the add_*_files sections below as needed
##     and list every .msg/.srv/.action file to be processed
##   * uncomment the generate_messages entry below
##   * add every package in MSG_DEP_SET to generate_messages(DEPENDENCIES ...)

## Generate messages in the 'msg' folder
add_message_files(
 FILES
 Key.msg
)

generate_messages(
 DEPENDENCIES
 std_msgs
 keyboard
)
## Generate services in the 'srv' folder
# add_service_files(
#   FILES
#   Service1.srv
#   Service2.srv
# )

## Generate actions in the 'action' folder
# add_action_files(
#   FILES
#   Action1.action
#   Action2.action
# )

## Generate added messages and services with any dependencies listed here
# generate_messages(
#   DEPENDENCIES
#   std_msgs
# )

################################################
## Declare ROS dynamic reconfigure parameters ##
################################################

## To declare and build dynamic reconfigure parameters within this
## package, follow these steps:
## * In the file package.xml:
##   * add a build_depend and a run_depend tag for "dynamic_reconfigure"
## * In this file (CMakeLists.txt):
##   * add "dynamic_reconfigure" to
##     find_package(catkin REQUIRED COMPONENTS ...)
##   * uncomment the "generate_dynamic_reconfigure_options" section below
##     and list every .cfg file to be processed

## Generate dynamic reconfigure parameters in the 'cfg' folder
# generate_dynamic_reconfigure_options(
#   cfg/DynReconf1.cfg
#   cfg/DynReconf2.cfg
# )

###################################
## catkin specific configuration ##
###################################
## The catkin_package macro generates cmake config files for your package
## Declare things to be passed to dependent projects
## INCLUDE_DIRS: uncomment this if you package contains header files
## LIBRARIES: libraries you create in this project that dependent projects also need
## CATKIN_DEPENDS: catkin_packages dependent projects also need
## DEPENDS: system dependencies of this project that dependent projects also need

catkin_package(
  INCLUDE_DIRS include
  CATKIN_DEPENDS roscpp rospy std_msgs message_runtime keyboard
  DEPENDS system_lib ${LIBS}
)

###########
## Build ##
###########

## Specify additional locations of header files
## Your package locations should be listed before other locations
# include_directories(include)
include_directories(
  ${catkin_INCLUDE_DIRS} ${SDL_INCLUDE_DIR})

link_directories ( ${Boost_LIBRARY_DIRS} )
include_directories ( ${Boost_INCLUDE_DIRS} )

## Declare a C++ library

add_library(bbo ${SHARED_OR_STATIC} ${bbo})
add_library(dmp ${SHARED_OR_STATIC} ${dmp})
add_library(dmp_bbo ${SHARED_OR_STATIC} ${dmp_bbo})
add_library(taskviapoint ${SHARED_OR_STATIC} ${TaskViapoint})
add_library(dynamicalsystems ${SHARED_OR_STATIC} ${dynamicalsystems})
add_library(functionapproximators ${SHARED_OR_STATIC} ${functionapproximators})

## Add cmake target dependencies of the library
## as an example, code may need to be generated before libraries
## either from message generation or dynamic reconfigure
# add_dependencies(dmp_bbo ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})

## Declare a C++ executable
#add_executable(dmp_ros_ctl src/dmp_ros_ctl.cpp)
#if changed you have to change it in target_link_libraries and install as well
add_executable(dmp_ros_ctl_ik src/dmp_ros_ctl_ik.cpp)
add_executable(dmp_ros_ctl_ik_train src/dmp_ros_ctl_ik_train.cpp)

## Add cmake target dependencies of the executable
## same as for the library above
# add_dependencies(dmp_bbo_node ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})

## Specify libraries to link a library or executable target against
 target_link_libraries(dmp_ros_ctl_ik taskviapoint dmp functionapproximators dynamicalsystems dmp_bbo bbo ${Boost_LIBRARIES}
  ${catkin_LIBRARIES} )
target_link_libraries(dmp_ros_ctl_ik_train taskviapoint dmp functionapproximators dynamicalsystems dmp_bbo bbo ${Boost_LIBRARIES}
  ${catkin_LIBRARIES} )
add_executable(keyboard src/main.cpp src/keyboard.cpp)

target_link_libraries(keyboard
 ${LIBS}
${catkin_LIBRARIES})

add_dependencies(keyboard keyboard_gencpp)


#############
## Install ##
#############

# all install targets should use catkin DESTINATION variables
# See http://ros.org/doc/api/catkin/html/adv_user_guide/variables.html

## Mark executable scripts (Python etc.) for installation
## in contrast to setup.py, you can choose the destination
# install(PROGRAMS
#   scripts/my_python_script
#   DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
# )

## Mark executables and/or libraries for installation
 install(TARGETS dmp_ros_ctl_ik dmp_ros_ctl_ik_train bbo dmp dmp_bbo taskviapoint dynamicalsystems functionapproximators keyboard
   ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
   LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
 )

## Mark cpp header files for installation
 
#install(DIRECTORY include/${PROJECT_NAME}/
   #DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
  # FILES_MATCHING PATTERN "*.h"
 #  PATTERN ".svn" EXCLUDE
# )
install(FILES ${HEADERS} DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION})

## Mark other files for installation (e.g. launch and bag files, etc.)
# install(FILES
#   # myfile1
#   # myfile2
#   DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
# )

#############
## Testing ##
#############

## Add gtest based cpp test target and link libraries
# catkin_add_gtest(${PROJECT_NAME}-test test/test_dmp_bbo.cpp)
# if(TARGET ${PROJECT_NAME}-test)
#   target_link_libraries(${PROJECT_NAME}-test ${PROJECT_NAME})
# endif()

## Add folders to be run by python nosetests
# catkin_add_nosetests(test)
