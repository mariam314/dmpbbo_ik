## \file demoExponentialSystem.py
## \author Freek Stulp
## \brief  Visualizes results of demoExponentialSystem.cpp
## 
## \ingroup Demos
## \ingroup DynamicalSystems

import matplotlib.pyplot as plt

import numpy
import os, sys, subprocess

# Include scripts for plotting
#lib_path = os.path.abspath('../../bbo/plotting')
#sys.path.append(lib_path)



if __name__=='__main__':

    executable2 = "/home/hrcstudents/Documents/Code/catkin_ws/devel/lib/dmp_bbo_node/keyboard"


    if (not os.path.isfile(executable2)):
        print ""
        print "ERROR: Executable '"+executable2+"' does not exist."
        print "Please call 'make install' in the build directory first."
        print ""
        sys.exit(-1);
    # Call the executable with the directory to which results should be written
   
    command = executable2
    print command
    subprocess.call(command, shell=True)
  

